# Testing Strategy
>
> Work In Progress !!! 
>

BankKata is following the [microservice testing philosophy](https://martinfowler.com/articles/microservice-testing/#conclusion-summary).
We will try here to explain how to get a cleaner test strategy in a microservice implemented according to the Hexagonal Architecture.

![Testing Strategy](images/testing-strategy.png)

## Functional Tests

>TODO
> 
### The anti-pattern

Working with Domain-Driven Design and the Hexagonal Architecture, usually means applying the Behavior-Driven Development methodology.
Many people have understood it and this technique is now widely spread. Unfortunately, we often see that the functional tests used to describe the behavior of modern application, are implemented as a http client hitting the endpoints of the controllers.
The main drawback of this (anti-)pattern is the mix of the testing concerns. With this kind of test, we have something that has the responsibility to verify:
* the business logic of the application *(functional tests)*
* the contract of the external API *(contract tests)*
* the workflow offered to the consumer - or something which looks like to it *(end to end tests)*
* sometimes the mapping between the domain objects and the adapters *(unit tests / integration tests)*
* the integration of the different sub-components of the application such as the Controllers, the DomainServices, the Repositories... *(integration tests / component tests)*
* and most of the time without knowing it: the living documentation *(contract testing)*

A simple serialization problem will fail this kind of test, and as a developer we will have to figure out if a regression has been made in the business logic or if something goes wrong with the configuration of a framework.
Which becomes a real hell when upgrading a structural framework like Spring Boot after playing search-and-destroy with the compilation errors...

The sustainability is not the only victim of this technique. Which framework to use when having a test which has that many responsibilities? [Cucumber](https://docs.cucumber.io/) might be one of the best choice to express the rules and the acceptance criteria of the business.
But testing the contract of a web API with it, can be really cu-cumbersome and the developer efficiency might be left behind.

### The Best practices

Back to the basics, since the aim of a functional test is "testing the business logic", putting it inside the domain of our application looks like a good idea. As a result instead of calling the endpoints, those tests are plugged in top of the API of the domain (not the Web API one).

![Functional Tests in the Hexagonal Architecture](images/hexagon-stubbed.png)

In BankKata, [Cucumber](https://docs.cucumber.io/) is used to define the features and the scenarios of our business logic. As you can see the feature files are located in [the tests packages of the domain](https://gitlab.emeadevops-loreal.com/demos/apps/java-demo/bank-kata/bank-kata-backend/-/tree/master/bankkata-domain/src/test/resources/features) beside the [step definitions](https://gitlab.emeadevops-loreal.com/demos/apps/java-demo/bank-kata/bank-kata-backend/-/tree/master/bankkata-domain/src/test/java/com/loreal/bankkata/acceptance/StepsDefinition).
Using the [Gherkin language](https://gitlab.emeadevops-loreal.com/demos/apps/java-demo/bank-kata/bank-kata-backend/-/tree/master/bankkata-domain/src/test/resources/features/consulting-history-transaction.feature):
```gherkin
# language: fr
Fonctionnalité: Consulter mon compte bancaire
  En tant que Client
  Afin de connaitre l'historique de mes transactions
  Je veux consulter mon compte

  Scénario: le client consulte l'historique de ses transactions
    Soit le client fait un dépot de "1000,00" euros le "01/12/2020"
    Et le client fait un retrait de "100,00" euros le "02/12/2020"
    Et le client fait un dépot de "500,00" euros le "10/12/2020"
    Quand le client consulte son compte
    Alors le système retourne l'historique de ses transactions:
      | Date       | Transaction | Solde    |
      | 10/12/2020 | 500,00      | 1400,00  |
      | 02/12/2020 | -100,00     |  900,00  |
      | 01/12/2020 | 1000,00     | 1000,00  |

```

Let's remind some best practices here:
* the scenario must be functional and [not technical](https://docs.cucumber.io/bdd/better-gherkin/)
* the persona must be defined ``En tant que client`` because a feature performed by different persona is really likely to have distinct acceptance criteria. Think about what differences you can have when an admin or an user are using the same feature
* Only **ONE** When, you should test one thing at a time, what's about the acceptance criteria with several whens ?
* Don't use conjunctions in your Givens and Thens, it makes them really hard to reuse, prefer to split your step into several ones

The step definitions are the fixtures of your test, they implement all the Gherkin's directives of your features [TransactionSteps.java](https://gitlab.emeadevops-loreal.com/demos/apps/java-demo/bank-kata/bank-kata-backend/-/tree/master/bankkata-domain/src/test/java/com/loreal/bankkata/acceptance/StepsDefinition/TransactionSteps.java):

```java
import com.kata.bankkata.domain.account.Account;
import com.kata.bankkata.domain.spi.TransactionRepository;
import com.kata.bankkata.domain.spi.stubs.FixedClock;
import com.kata.bankkata.domain.transaction.TransactionFormatter;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Et;
import io.cucumber.java.fr.Quand;
import io.cucumber.java.fr.Soit;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class TransactionSteps {

    private static final NumberFormat nFormat = NumberFormat.getInstance(Locale.FRENCH);
    private final Account account;
    private final TransactionFormatter transactionFormatter;
    private final FixedClock clock;

    TransactionSteps(TransactionRepository transactionRepository, FixedClock clock) {
        this.clock = clock;
        this.transactionFormatter = new TransactionFormatter();
        account = new Account(transactionRepository, transactionFormatter);
    }

    @Soit("le client fait un dépot de {string} euros le {string}")
    public void le_client_fait_un_depot(String transaction, String date) throws Exception {
        clock.setTestDate(date);
        int amount = nFormat.parse(transaction).intValue();
        account.deposit(amount);
    }

    @Et("le client fait un retrait de {string} euros le {string}")
    public void le_client_fait_un_retrait(String transaction, String date) throws Exception {
        clock.setTestDate(date);
        int amount = nFormat.parse(transaction).intValue();
        account.withdraw(amount);
    }

    @Quand("le client consulte son compte")
    public void le_client_consulte_son_compte() throws Exception {
        account.consultHistory();
    }

    @Alors("le système retourne l'historique de ses transactions:")
    public void le_systeme_retourne_lhistorique_des_transactions_du_compte(DataTable transactions) throws Exception {
        List<List<String>> expected = transactions.asLists(String.class);

        List<List<String>> results = transactionFormatter.getHistoryTransactions();

        assertEquals(expected, results);
    }
}
```

Like said above, instead of hitting the web endpoints of the application, we are using directly here the domain API, through [ConsultHistory](https://gitlab.emeadevops-loreal.com/demos/apps/java-demo/bank-kata/bank-kata-backend/-/blob/master/bankkata-domain/src/main/java/com/loreal/bankkata/domain/api/ConsultHistory.java) - a domain service.
So basically all the Whens of our step definitions will call the methods of the domain services exposed through the API = **the features**. In the Givens and the Thens, we respectively creates and asserts on the domain objects. And we also bootstrap or assert some data through the SPI.

Most of the time, users split their step definitions in the same way as their scenarios. As you can read from the [cucumber documentation itself](https://docs.cucumber.io/guides/anti-patterns/), it couples everything together and the steps you made cannot be reused for another scenario.
The step definitions should be organised in the same way than your domain concepts and, moreover, named after them. This way your step definitions code will be factorized, easier to find and navigate through, you'll get easier steps to manage and reuse. In other words, **the step definitions should be designed with the same processes we use to create our business domain!**

### Domain Stubs

While implementing a feature, we often need to call some external dependencies which offers a service to our domain.
But since the domain cannot depends on external stuff, how can we test it ? The response is pretty simple, we creates a lightweight component which implements the SPI **inside the domain** [InMemoryTransactionRepository](https://gitlab.emeadevops-loreal.com/demos/apps/java-demo/bank-kata/bank-kata-backend/-/blob/master/bankkata-domain/src/main/java/com/loreal/bankkata/domain/spi//stubs/InMemoryTransactionRepository.java).
This will ensure the inside of the hexagon - the domain of our application - will be self-tested.
Why not only a Mock inside the tests ? The domain stub is more that just a testing tool, it helps you to focus on the important thing of your development process.

![Functional Tests in the Hexagonal Architecture](images/hexagon-stubbed.png)

### Low-Level Assertions Caveat

>TODO

## Unit Tests

>TODO
> 
## Integration Tests, Contract Testing & End-To-End Tests

>TODO

## Documentation

More than a specification, the feature tests written in Gherkin is a real living documentation of your application. It should reflects what is the behavior of a feature in a given scenario, pretty helpful when you need to improve or maintain the domain.
That's why we strongly recommend to make sure the Cucumber tests will fails if your step definitions are not implemented. **Having a confusing (lying) documentation is worse than not having documentation at all!**


