FROM openjdk:11
VOLUME /tmp
ADD bankkata-infra-application/target/bankkata-infra-application-0.0.1-SNAPSHOT-exec.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]