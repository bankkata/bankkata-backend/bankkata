# language: fr
Fonctionnalité: Consulter mon compte bancaire
  En tant que Client
  Afin de connaitre l'historique de mes transactions
  Je veux consulter mon compte

  Scénario: le client consulte l'historique de ses transactions
    Soit le client fait un dépot de "1000,00" euros le "01/12/2020"
    Et le client fait un retrait de "100,00" euros le "02/12/2020"
    Et le client fait un dépot de "500,00" euros le "10/12/2020"
    Quand le client consulte son compte
    Alors le système retourne l'historique de ses transactions:
      | Date       | Transaction | Solde    |
      | 10/12/2020 | 500,00      | 1400,00  |
      | 02/12/2020 | -100,00     |  900,00  |
      | 01/12/2020 | 1000,00     | 1000,00  |
