package com.kata.bankkata.acceptance.StepsDefinition;

import com.kata.bankkata.domain.account.Account;
import com.kata.bankkata.domain.spi.TransactionRepository;
import com.kata.bankkata.domain.spi.stubs.FixedClock;
import com.kata.bankkata.domain.transaction.TransactionFormatter;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Et;
import io.cucumber.java.fr.Quand;
import io.cucumber.java.fr.Soit;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class TransactionSteps {

    private static final NumberFormat nFormat = NumberFormat.getInstance(Locale.FRENCH);
    private final Account account;
    private final TransactionFormatter transactionFormatter;
    private final FixedClock clock;

    TransactionSteps(TransactionRepository transactionRepository, FixedClock clock) {
        this.clock = clock;
        this.transactionFormatter = new TransactionFormatter();
        account = new Account(transactionRepository, transactionFormatter);
    }

    @Soit("le client fait un dépot de {string} euros le {string}")
    public void le_client_fait_un_depot(String transaction, String date) throws Exception {
        clock.setTestDate(date);
        int amount = nFormat.parse(transaction).intValue();
        account.deposit(amount);
    }

    @Et("le client fait un retrait de {string} euros le {string}")
    public void le_client_fait_un_retrait(String transaction, String date) throws Exception {
        clock.setTestDate(date);
        int amount = nFormat.parse(transaction).intValue();
        account.withdraw(amount);
    }

    @Quand("le client consulte son compte")
    public void le_client_consulte_son_compte() throws Exception {
        account.consultHistory();
    }

    @Alors("le système retourne l'historique de ses transactions:")
    public void le_systeme_retourne_lhistorique_des_transactions_du_compte(DataTable transactions) throws Exception {
        List<List<String>> expected = transactions.asLists(String.class);

        List<List<String>> results = transactionFormatter.getHistoryTransactions();

        assertEquals(expected, results);
    }
}
