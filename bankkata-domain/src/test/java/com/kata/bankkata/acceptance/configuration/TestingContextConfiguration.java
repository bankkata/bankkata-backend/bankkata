package com.kata.bankkata.acceptance.configuration;

import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.test.context.ContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration(classes = {
        RepositoriesConfiguration.class
})
public class TestingContextConfiguration {

}
