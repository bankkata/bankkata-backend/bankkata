package com.kata.bankkata.acceptance;

import com.kata.bankkata.domain.Clock;
import com.kata.bankkata.domain.account.Account;
import com.kata.bankkata.domain.spi.TransactionRepository;
import com.kata.bankkata.domain.spi.stubs.InMemoryTransactionRepository;
import com.kata.bankkata.domain.transaction.TransactionFormatter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ConsultingTransaction {

    private static final List<String> HEADER = asList("Date", "Transaction", "Solde");
    private static final List<String> SECOND_DEPOSIT = asList("10/12/2020", "500,00", "1400,00");
    private static final List<String> WITHDRAW = asList("02/12/2020", "-100,00", "900,00");
    private static final List<String> FIRST_DEPOSIT = asList("01/12/2020", "1000,00", "1000,00");

    @Mock Clock clock;

    private Account account;
    private TransactionFormatter transactionFormatter;

    @BeforeEach
    public void setup() {
        TransactionRepository transactionRepository = new InMemoryTransactionRepository(clock);
        transactionFormatter = new TransactionFormatter();
        account = new Account(transactionRepository, transactionFormatter);
    }

    @Test
    void should_return_history() {
        List<List<String>> expected = asList(HEADER, SECOND_DEPOSIT, WITHDRAW, FIRST_DEPOSIT);

        given(clock.todayAsString()).willReturn("01/12/2020", "02/12/2020", "10/12/2020");

        account.deposit(1000);
        account.withdraw(100);
        account.deposit(500);

        account.consultHistory();

        List<List<String>> results = transactionFormatter.getHistoryTransactions();

        assertEquals(expected, results);
    }

}
