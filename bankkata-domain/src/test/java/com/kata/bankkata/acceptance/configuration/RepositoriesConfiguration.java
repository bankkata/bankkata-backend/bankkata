package com.kata.bankkata.acceptance.configuration;

import com.kata.bankkata.domain.Clock;
import com.kata.bankkata.domain.spi.TransactionRepository;
import com.kata.bankkata.domain.spi.stubs.FixedClock;
import com.kata.bankkata.domain.spi.stubs.InMemoryTransactionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class RepositoriesConfiguration {

    @Bean
    @Scope("cucumber-glue")
    public Clock clock() {
        return new FixedClock();
    }

    @Bean
    @Scope("cucumber-glue")
    public TransactionRepository transactionRepository() {
        return new InMemoryTransactionRepository(clock());
    }
}