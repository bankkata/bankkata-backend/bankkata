package com.kata.bankkata.domain.transaction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TransactionFormatterShould {

    private static final List<Transaction> NO_TRANSACTIONS = emptyList();
    private final List<List<String>> expected = new ArrayList<List<String>>();

    private TransactionFormatter transactionFormatter;

    @BeforeEach
    public void setUp() throws Exception {
        transactionFormatter = new TransactionFormatter();
    }

    @Test
    void always_format_the_header() {
        expected.add(asList("Date", "Transaction", "Solde"));

        transactionFormatter.format(NO_TRANSACTIONS);

        assertEquals(expected, transactionFormatter.getHistoryTransactions());
    }

    @Test
    void format_transaction_in_reverse_chronological_order() {

        expected.add(asList("Date", "Transaction", "Solde"));
        expected.add(asList("10/12/2020","500,00","1400,00"));
        expected.add(asList("02/12/2020","-100,00","900,00"));
        expected.add(asList("01/12/2020","1000,00","1000,00"));


        transactionFormatter.format(transactionsContaining(
                deposit("01/12/2020", 1000),
                withdraw("02/12/2020", 100),
                deposit("10/12/2020", 500)
        ));

        assertEquals(expected, transactionFormatter.getHistoryTransactions());
    }

    private List<Transaction> transactionsContaining(Transaction... transactions) {
        return asList(transactions);
    }

    private Transaction withdraw(String date, int amount) {
        return new Transaction(date, -amount);
    }

    private Transaction deposit(String date, int amount) {
        return new Transaction(date, amount);
    }

}