package com.kata.bankkata.domain.account;

import com.kata.bankkata.domain.spi.TransactionRepository;
import com.kata.bankkata.domain.transaction.Transaction;
import com.kata.bankkata.domain.transaction.TransactionFormatter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static java.util.Arrays.asList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AccountShould {

    @Mock
    TransactionRepository transactionRepository;

    @Mock
    TransactionFormatter transactionFormatter;

    private Account account;

    @BeforeEach
    public void setUp() {
        account = new Account(transactionRepository, transactionFormatter);
    }

    //Differ details of a transaction to repository rather than in account class
    @Test
    void store_a_deposit_transaction() {
        account.deposit(100);
        verify(transactionRepository).addDeposit(100);
    }

    @Test
    void store_a_withdrawal_transaction() {
        account.withdraw(100);
        verify(transactionRepository).addWithdrawal(100);
    }

    @Test
    void format_transactions() {
        List<Transaction> transactions = asList(new Transaction("21/12/2020",100));
        given(transactionRepository.allTransactions()).willReturn(transactions);

        account.consultHistory();

        verify(transactionFormatter).format(transactions);
    }
}