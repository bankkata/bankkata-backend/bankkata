package com.kata.bankkata.domain.repositories;

import com.kata.bankkata.domain.Clock;
import com.kata.bankkata.domain.spi.TransactionRepository;
import com.kata.bankkata.domain.spi.stubs.InMemoryTransactionRepository;
import com.kata.bankkata.domain.transaction.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

// This test class is a stub that drives the design of persistency contract.
// Using a stub instead of the final implementation helps the developer to focus
// on domain implementation without the need of a database.
// A concrete implementation of the spi is provided within the infrastructure layer.
@ExtendWith(MockitoExtension.class)
class TransactionRepositoryShould {

    @Mock Clock clock;

    public static final String TODAY = "21/12/2020";

    private TransactionRepository transactionRepository;

    @BeforeEach
    public void setUp() {
        transactionRepository  = new InMemoryTransactionRepository(clock);
        given(clock.todayAsString()).willReturn(TODAY);
    }

    @Test
    void create_and_store_deposit_transaction() {

        transactionRepository.addDeposit(100);

        List<Transaction> transactions = transactionRepository.allTransactions();

        assertEquals(1, transactions.size());
        assertEquals(transaction(TODAY, 100), transactions.get(0));
    }

    @Test
    void create_and_store_withdrawal_transaction() {
        transactionRepository.addWithdrawal(100);

        List<Transaction> transactions = transactionRepository.allTransactions();

        assertEquals(1, transactions.size());
        assertEquals(transaction(TODAY, -100), transactions.get(0));
    }

    private Transaction transaction(String date, int amount) {
        return new Transaction(date, amount);
    }
}