package com.kata.bankkata.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClockShould {

    @Test
    void return_todays_date_in_DD_MM_YYYY_format() {
        Clock clock = new TestableClock();

        final String date = clock.todayAsString();

        assertEquals("15/12/2020", date);
    }

    private class TestableClock extends Clock {
        @Override
        protected LocalDate today() {
            return LocalDate.of(2020,12,15);
        }
    }
}