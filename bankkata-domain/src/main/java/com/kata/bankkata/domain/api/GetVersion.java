package com.kata.bankkata.domain.api;

public interface GetVersion {
    String forApplication();
}
