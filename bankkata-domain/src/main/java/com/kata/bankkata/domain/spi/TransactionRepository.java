package com.kata.bankkata.domain.spi;

import com.kata.bankkata.domain.transaction.Transaction;

import java.util.List;

public interface TransactionRepository {
    void addDeposit(int amount);

    void addWithdrawal(int amount);

    List<Transaction> allTransactions();
}
