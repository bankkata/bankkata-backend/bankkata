package com.kata.bankkata.domain.spi.stubs;

import com.kata.bankkata.domain.transaction.Transaction;
import com.kata.bankkata.domain.Clock;
import com.kata.bankkata.domain.spi.TransactionRepository;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class InMemoryTransactionRepository implements TransactionRepository {

    private final List<Transaction> transactions = new ArrayList<>();
    private final Clock clock;

    public InMemoryTransactionRepository(Clock clock) {
        this.clock = clock;
    }

    @Override
    public void addDeposit(int amount) {
        final Transaction deposit = new Transaction(clock.todayAsString(), amount);
        transactions.add(deposit);
    }

    @Override
    public void addWithdrawal(int amount) {
        final Transaction withdrawal = new Transaction(clock.todayAsString(), -amount);
        transactions.add(withdrawal);
    }

    @Override
    public List<Transaction> allTransactions() {
        return unmodifiableList(transactions);
    }
}
