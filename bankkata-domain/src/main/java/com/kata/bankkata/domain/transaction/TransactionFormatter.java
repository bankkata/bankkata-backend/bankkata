package com.kata.bankkata.domain.transaction;

import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Component
public class TransactionFormatter {

    private static final List<String> HEADER = asList("Date", "Transaction", "Solde");

    private final List<List<String>> formattedTransactions = new LinkedList<>();
    private final DecimalFormat decimalFormatter = new DecimalFormat("#.00", new DecimalFormatSymbols(Locale.FRANCE));

    public List<List<String>> getHistoryTransactions() {
        return formattedTransactions;
    }

    public void format(List<Transaction> transactions) {
        formattedTransactions.add(HEADER);
        addHistory(transactions);
    }

    private void addHistory(List<Transaction> transactions) {
        AtomicInteger solde = new AtomicInteger();

        transactions.stream()
                .map(transaction -> historyList(transaction, solde))
                .collect(Collectors.toCollection(LinkedList::new))
                .descendingIterator()
                .forEachRemaining(formattedTransactions::add);
    }

    private List<String> historyList(Transaction transaction, AtomicInteger solde) {
        return asList(
                transaction.date(),
                decimalFormatter.format(transaction.amount()),
                decimalFormatter.format(solde.addAndGet(transaction.amount()))
        );
    }
}


