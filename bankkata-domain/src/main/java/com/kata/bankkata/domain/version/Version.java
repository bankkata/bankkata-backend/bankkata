package com.kata.bankkata.domain.version;

import com.kata.bankkata.domain.api.GetVersion;
import org.springframework.stereotype.Service;

@Service
public class Version implements GetVersion {
    private static final String APP_VERSION = "Bank Kata Version 1.0";

    public String forApplication() {
        return APP_VERSION;
    }
}
