package com.kata.bankkata.domain.api;

public interface Withdraw {
    void withdraw(int amount);
}
