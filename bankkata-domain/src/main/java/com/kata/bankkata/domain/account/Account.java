package com.kata.bankkata.domain.account;

import com.kata.bankkata.domain.transaction.TransactionFormatter;
import com.kata.bankkata.domain.api.ConsultHistory;
import com.kata.bankkata.domain.api.Deposit;
import com.kata.bankkata.domain.api.Withdraw;
import com.kata.bankkata.domain.spi.TransactionRepository;
import org.springframework.stereotype.Service;

@Service
public class Account implements Deposit, Withdraw, ConsultHistory {
    private final TransactionRepository transactionRepository;
    private final TransactionFormatter transactionFormatter;

    public Account(TransactionRepository transactionRepository, TransactionFormatter transactionFormatter) {
        this.transactionRepository = transactionRepository;
        this.transactionFormatter = transactionFormatter;
    }

    @Override
    public void deposit(int amount) {
        transactionRepository.addDeposit(amount);
    }

    @Override
    public void withdraw(int amount){
        transactionRepository.addWithdrawal(amount);
    }

    @Override
    public void consultHistory(){
        transactionFormatter.format(transactionRepository.allTransactions());
    }
}
