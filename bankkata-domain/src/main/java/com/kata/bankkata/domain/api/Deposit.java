package com.kata.bankkata.domain.api;

public interface Deposit {
    void deposit(int amount);
}
