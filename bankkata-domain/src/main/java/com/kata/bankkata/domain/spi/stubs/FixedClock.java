package com.kata.bankkata.domain.spi.stubs;

import com.kata.bankkata.domain.Clock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class FixedClock extends Clock {

    private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    private int year;
    private int month;
    private int day;

    public void setTestDate(String testDate) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
        try {
            Date d = formatter.parse(testDate);
            cal.setTime(d);
            year = cal.get(Calendar.YEAR);
            month = cal.get(Calendar.MONTH) + 1;
            day = cal.get(Calendar.DAY_OF_MONTH);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected LocalDate today() {
        return LocalDate.of(year, month, day);
    }
}
