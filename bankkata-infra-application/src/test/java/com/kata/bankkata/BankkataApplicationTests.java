package com.kata.bankkata;

import com.kata.bankkata.controllers.DepositController;
import com.kata.bankkata.controllers.HomeController;
import com.kata.bankkata.controllers.VersionController;
import com.kata.bankkata.domain.transaction.TransactionFormatter;
import com.kata.bankkata.repository.H2TransactionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BankkataApplicationTests {

    @Autowired
    private HomeController homeController;

    @Autowired
    VersionController versionController;

    @Autowired
    DepositController depositController;

    @Autowired
    H2TransactionRepository transactionJpaRepository;

    @Autowired
    TransactionFormatter transactionFormatter;

    @Test
    void contextLoads() {
        assertThat(homeController).isNotNull();
        assertThat(versionController).isNotNull();
        assertThat(depositController).isNotNull();
        assertThat(transactionJpaRepository).isNotNull();
        assertThat(transactionFormatter).isNotNull();
    }
}
