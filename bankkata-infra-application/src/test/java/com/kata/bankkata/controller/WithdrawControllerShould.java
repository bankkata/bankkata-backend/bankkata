package com.kata.bankkata.controller;

import com.kata.bankkata.controllers.WithdrawController;
import com.kata.bankkata.domain.api.Withdraw;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WithdrawController.class)
class WithdrawControllerShould {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Withdraw withdraw;

    @Test
    void withdraw_an_amount() throws Exception {
        this.mockMvc
                .perform(post("/withdraw?amount=500"))
                .andExpect(status().isOk());
    }
}
