package com.kata.bankkata.controller;

import com.kata.bankkata.controllers.DepositController;
import com.kata.bankkata.domain.api.Deposit;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DepositController.class)
class DepositControllerShould {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Deposit deposit;

    @Test
    void deposit_an_amount() throws Exception {

        this.mockMvc
                .perform(post("/deposit?amount=100"))
                .andExpect(status().isOk());
    }
}
