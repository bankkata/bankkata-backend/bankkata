package com.kata.bankkata.controller;

import com.kata.bankkata.controllers.VersionController;
import com.kata.bankkata.domain.api.GetVersion;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(VersionController.class)
class WebLayerWithMockedVersionServiceShould {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetVersion getVersionService;

    @Test
    void returnCurrentVersion() throws Exception {
        when(getVersionService.forApplication()).thenReturn("1.0");

        this.mockMvc
                .perform(get("/version"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("1.0")));
    }
}
