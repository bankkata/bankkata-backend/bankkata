package com.kata.bankkata.model;

import com.kata.bankkata.repository.H2TransactionRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
class TransactionEntityShould {

    private static final Logger logger = LoggerFactory.getLogger(TransactionEntityShould.class);

    @Autowired
    private H2TransactionRepository h2TransactionRepository;

    @Test
    @Rollback(value = false)
    void create_transaction() {
        TransactionEntity transactionEntity = new TransactionEntity("10/02/2021", 1000);

        TransactionEntity savedTransactionEntity = h2TransactionRepository.save(transactionEntity);

        assertThat(savedTransactionEntity).isNotNull();
    }

    @Test
    void list_all_transactions() {

        List<TransactionEntity> transactionEntities = h2TransactionRepository.findAll();

        for (TransactionEntity transactionEntity : transactionEntities) {
            logger.info(transactionEntity.toString());
        }

        Assertions.assertThat(transactionEntities).size().isGreaterThan(0);
    }

}
