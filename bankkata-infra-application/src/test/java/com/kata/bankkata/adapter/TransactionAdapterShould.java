package com.kata.bankkata.adapter;

import com.kata.bankkata.domain.spi.TransactionRepository;
import com.kata.bankkata.domain.transaction.Transaction;
import com.kata.bankkata.model.TransactionEntity;
import com.kata.bankkata.repository.H2TransactionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {
        TransactionAdapter.class,
        TransactionRepository.class})
class TransactionAdapterShould {


    @Autowired
    private TransactionRepository transactionRepository;

    @MockBean
    private H2TransactionRepository transactionJpaRepository;

    @Captor
    private ArgumentCaptor<TransactionEntity> transactionEntityArgumentCaptor;

    @Test
    void port_deposit_creation_and_storage_to_repository() {

       transactionRepository.addDeposit(1000);

       verify(transactionJpaRepository, only()).save(transactionEntityArgumentCaptor.capture());
       final TransactionEntity transactionEntity = transactionEntityArgumentCaptor.getValue();
       assertEquals(1000, transactionEntity.getAmount());
    }

    @Test
    void port_withdraw_creation_and_storage_to_repository() {
        transactionRepository.addWithdrawal(500);

        verify(transactionJpaRepository, only()).save(transactionEntityArgumentCaptor.capture());
        final TransactionEntity transactionEntity = transactionEntityArgumentCaptor.getValue();
        assertEquals(-500, transactionEntity.getAmount());
    }

    @Test
    void port_list_all_transactions_to_repository_and_return_adapted_transaction() {

        final TransactionEntity transactionEntity = new TransactionEntity("09/02/2020",1000);
        final List<TransactionEntity> transactionEntities = Collections.singletonList(transactionEntity);
        when(transactionJpaRepository.findAll()).thenReturn(transactionEntities);

        final List<Transaction> transactions = transactionRepository.allTransactions();

        verify(transactionJpaRepository, only()).findAll();
        assertThat(transactions).hasSize(1);
        final Transaction transaction = transactions.get(0);
        assertThat(transaction.date()).isEqualTo("09/02/2020");
        assertThat(transaction.amount()).isEqualTo(1000);
    }
}
