package com.kata.bankkata.controllers;

import com.kata.bankkata.domain.api.GetVersion;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {

    private final GetVersion version;

    VersionController(GetVersion version) {
        this.version = version;
    }

    @RequestMapping("/version")
    public @ResponseBody String getVersion() {
        return version.forApplication();
    }
}
