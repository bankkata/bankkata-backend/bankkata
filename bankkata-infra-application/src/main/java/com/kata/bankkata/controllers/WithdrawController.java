package com.kata.bankkata.controllers;

import com.kata.bankkata.domain.api.Withdraw;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WithdrawController {
    private final Withdraw withdraw;

    public WithdrawController(Withdraw withdraw) {
        this.withdraw = withdraw;
    }

    @PostMapping("/withdraw")
    @ResponseStatus(HttpStatus.OK)
    public void addWithdraw(@RequestParam("amount") int amount) {
        withdraw.withdraw(amount);
    }
}
