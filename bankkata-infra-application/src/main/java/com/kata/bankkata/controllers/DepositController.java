package com.kata.bankkata.controllers;

import com.kata.bankkata.domain.api.Deposit;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class DepositController {

    private final Deposit deposit;

    public DepositController(Deposit deposit) {
        this.deposit = deposit;
    }

    @PostMapping("/deposit")
    @ResponseStatus(HttpStatus.OK)
    public void addDeposit(@RequestParam("amount") int amount) {
        deposit.deposit(amount);
    }
}
