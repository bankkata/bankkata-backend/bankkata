package com.kata.bankkata.adapter;

import com.kata.bankkata.domain.Clock;
import com.kata.bankkata.domain.spi.TransactionRepository;
import com.kata.bankkata.domain.transaction.Transaction;
import com.kata.bankkata.model.TransactionEntity;
import com.kata.bankkata.repository.H2TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionAdapter implements TransactionRepository {

    private H2TransactionRepository transactionJpaRepository;

    private Clock clock;

    public TransactionAdapter(H2TransactionRepository transactionJpaRepository) {
        this.transactionJpaRepository = transactionJpaRepository;
        this.clock = new Clock();
    }

    @Override
    public void addDeposit(int amount) {
        TransactionEntity transactionEntity = new TransactionEntity(clock.todayAsString(), amount);
        transactionJpaRepository.save(transactionEntity);
    }

    @Override
    public void addWithdrawal(int amount) {
        TransactionEntity transactionEntity = new TransactionEntity(clock.todayAsString(), -amount);
        transactionJpaRepository.save(transactionEntity);
    }

    @Override
    public List<Transaction> allTransactions() {
        return transactionJpaRepository.findAll()
                .stream()
                .map(this::transactionEntityToTransaction)
                .collect(Collectors.toList());
    }

    private Transaction transactionEntityToTransaction(TransactionEntity transactionEntity) {
        return new Transaction(transactionEntity.getDate(), transactionEntity.getAmount());
    }
}
