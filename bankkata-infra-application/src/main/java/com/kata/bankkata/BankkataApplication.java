package com.kata.bankkata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankkataApplication {

    private static final Logger logger = LoggerFactory.getLogger(BankkataApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BankkataApplication.class, args);
        logger.info("BankkataApplication started...");
    }

}
