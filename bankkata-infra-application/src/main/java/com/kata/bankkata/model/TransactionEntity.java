package com.kata.bankkata.model;

import javax.persistence.*;

@Entity
@Table(name = "Transactions")
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 64)
    private String date;

    @Column(nullable = false)
    private int amount;

    public TransactionEntity(String date, int amount) {
        this.date = date;
        this.amount = amount;
    }

    public TransactionEntity() {
    }

    public String getDate() {
        return date;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "TransactionEntity{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", amount=" + amount +
                '}';
    }
}
