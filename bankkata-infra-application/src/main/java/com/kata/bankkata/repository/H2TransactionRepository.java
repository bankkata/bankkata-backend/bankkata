package com.kata.bankkata.repository;

import com.kata.bankkata.model.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface H2TransactionRepository extends JpaRepository<TransactionEntity, Long> {
}
