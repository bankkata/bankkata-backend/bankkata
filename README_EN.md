# Bank Kata
[![pipeline status](https://gitlab.com/bankkata/bankkata-backend/bankkata/badges/master/pipeline.svg)](https://gitlab.com/bankkata/bankkata-backend/bankkata/commits/master)

Bank Kata is a [hexagonal architecture](https://alistair.cockburn.us/hexagonal-architecture/) demo application developed with Java and SpringBoot.

![Hexagonal Architecture](images/hexagonal-architecture.png)

## Build

To build BankKata, run the following command:
```
mvn clean install
```

## Running the application
### Production mode

>TODO

### Development mode

>TODO

## Living Documentation

>TODO

## Technical Overview

Bank Kata is composed of 2 different modules:

### bankkata-domain the inside of the hexagon

>TODO

#### bankkata-infra-application

>TODO

## Testing Strategy

If you wan to learn more on the testing strategy applied in Bank Kata, [here](TestingStrategy_EN.md) is the dedicated documentation.

## Contributors

Christophe THÉPAUT - @christophe.thepaut



